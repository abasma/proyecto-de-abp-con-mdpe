//LIBRERIAS
#include <Arduino.h>
#include <LiquidCrystal.h>
#include <Keypad.h>

//MACROS 
#define SensorHumedad A1
#define PinTrig 18        //Pin Ultrasonido
#define PinEcho 19        //Pin Ultrasonido
#define LEDVALV 14        //Pin de la valvula de la huerta + su pin testigo
#define LEDTANQUE 17      //Pin de la valvula de la huerta + su pin testigo
#define SoundSpeed 34000  //Velocidad del sonido en cm/s
#define numLect 50        //Cant de lecturas a tomar
#define DistanciaTot ???  //Distancia del sensor al fondo del tanque(vacio)
#define Distancia100 ???  //Distancia del sensor al tanque con 1L de liquido

//VARIABLES
int humedad = 0;
int nivelagua = 0;
volatile bool estado = 0;

float lecturas[numLect];  //Array para cuentas
int LectActual = 0;       
float total = 0;          //Lecturas totales
float media = 0;          //Media de las medidas
bool PrimerMed = false;   //¿Ya hiciste las primeras "numLect" medidas?

//PANTALLA LCD
const int rs = 11, en = 13, d4 = 5, d5 = 4, d6 = 3, d7 = 2; //Pines de la pantalla LCD
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

//PULSADORES
const unsigned char ROWS = 2;
const unsigned char COLS = 2;
char keys[ROWS][COLS] = {
  {'REGAR','AGUADECASA'},
  {'HUMEDAD','NIVELDEAGUA'}
 
};

unsigned char rowPins[ROWS]= {6, 7};
unsigned char colPins[COLS]= {8, 9};

	Keypad kpd = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS);

void setup() {
  //Modo de trabajo de los pines
  pinMode(SensorHumedad, OUTPUT);
  pinMode(SensorNivel, OUTPUT);
  pinMode(PinTrig, OUTPUT);
  pinMode(PinEcho, INPUT);
  pinMode(LEDVALV, INPUT);
  pinMode(LEDTANQUE, INPUT);
  //Los interrupt utilizados
  attachInterrupt(digitalPinToInterrupt('REGAR'), regar, HIGH); //Cuando esta en alto
  attachInterrupt(digitalPinToInterrupt('AGUADECASA'), cambiartanque, RISING); //En flanco de subida
  //Inicializamos el array
  for(int i=0; i<numLect; i++){
    lecturas[i] = 0;
  }
  //inicializamos la pantalla LCD
  lcd.begin(16, 2);
}

void loop() {
  total = total - lecturas[LectActual]; //Eliminamos las medidas anteriores

  iniciarTrigger(); //Inicializamos el trigger

  unsigned long tiempo = pulseIn(pinEcho, HIGH); //Guardamos en tiempo lo que tardo echo en cambiar de estado
  float distancia = tiempo * 0.000001 * SoundSpeed / 2; //Distancia en cm
  lecturas[LectActual] = distancia; //Guardamos la distancia en el array
  total = total + lecturas[LectActual]; //Añadimos la lectura actual al total
  LectActual = LectActual + 1; //Aumentamos en 1 la lectura actual

  //Vemos si hicimos la cantidad de lecturas especificadas y reseteamos LectActual
  if (LectActual >= numLect) {
    PrimerMed = TRUE;
    LectActual = 0;
  }
  
  media = total / numLect; //el valor medio entre todas las lecturas realizadas

  }

  char key = kpd.getKey(); //guardamos en key la tecla presionada
    if (key != NO_KEY){    //Vemos si hay alguna tecla presionada
      switch (key){
        //Vemos los posibles casos te teclas presionadas 
        case 'HUMEDAD':
          humedad = map(analogRead(SensorHumedad), 0, 1023, 100, 0); //guardamos en humedad la lectura analoga %
          lcd.clear(); //Limpiamos la pantalla LCD
          lcd.setCursor(3, 0); //Seleccionamos donde comenzar a imprimir
          lcd.print("hay un "); //Imprimimos un texto
          lcd.print(humedad);   //Imprimimos una variable
          lcd.print("%  ");
          lcd.setCursor(3, 1); 
          lcd.print("de humedad");
        break; //salimos del caso(infinito)
         
        case 'NIVELDEAGUA':
          if (PrimerMed){ //Por lo menos una medida
            float ContenidoDist = DistanciaTot - media; //La altura(vacio) - la altura(con liquido)= Altura del contenido desde el piso
            float Liquido = ContenidoDist * 100 / Distancia100; 
            int porcentaje = (int) (ContenidoDist * 100 / Distancia100);
            lcd.clear();
            lcd.setCursor(3, 0); 
            lcd.print("Esta un ");
            lcd.print(porcentaje);
            lcd.print("% lleno");
            lcd.setCursor(3, 1); 
            lcd.print(string(Liquido) + "mL");
        break;
    }

  if (humedad<=40) {
   digitalWrite(LEDVALV, HIGH);
   //este 1 se transforme en 0
  }
  else {
    digitalWrite(LEDVALV, LOW);
    //este 0 se transforme en 1
  }
  if (ContenidoDist <= Distancia100) {
   digitalWrite(LEDTANQUE, HIGH);
  }
  else {
    digitalWrite(LEDTANQUE, LOW);
  }
  
}

void regar(){
  KeyState presionado = keypad.getState('REGAR');
  if (presionado == HOLD){
    digitalWrite(LEDVALV, HIGH);
  }
  digitalWrite(LEDVALV, LOW);
}

void cambiartanque(){
    estado = !estado;
    digitalWrite(LEDTANQUE, estado);
}

void iniciarTrigger(){
  digitalWrite(PinTrig, LOW);
  delay(2);

  digitalWrite(PinTrig, HIGH;
  delay(10);

  digitalWrite(PinTrig, LOW);
}